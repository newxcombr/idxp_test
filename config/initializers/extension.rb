class String
  def to_macaddress
    self.strip.scan(/.{1,2}/).join(":")
  end
end