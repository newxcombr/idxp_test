class Node < ActiveRecord::Base

  validates_presence_of :mac, :store_id

  belongs_to :store
  has_many :store_area_nodes

end
