class StoreAreaNode < ActiveRecord::Base

  belongs_to :store_area
  belongs_to :node

  validates_presence_of :node, :store_area

end
