class Store < ActiveRecord::Base

  belongs_to :chain
  has_many :store_areas
  has_many :store_area_nodes, through: :store_areas

end
