class StoreArea < ActiveRecord::Base

  belongs_to :store
  has_many :store_area_nodes
  validates_presence_of :store_id, :area_id

end

