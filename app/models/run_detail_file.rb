require 'csv'
require 'date'

class RunDetailFile < ActiveRecord::Base

  belongs_to :store_area_node
  belongs_to :store_area

  def self.load_from_csv(csv_path, threshold = -60.0)
    imported = 0

    opts = {:headers => false, :return_headers => false}
    CSV.foreach(csv_path, opts) do |row|
        begin
            unixtime, mac, diff = row[0], row[5], row[6].to_f
            datetime = DateTime.strptime(unixtime,'%s').strftime('%Y-%m-%d %H:%M:%S')
            node = Node.find_by_mac(mac.to_macaddress)

            if !node.nil? and diff >= threshold then
                create(
                    enter: datetime,
                    area: node.description,
                    store_name: node.store.name
                )
                imported += 1
            end
        rescue Exception => e 
            puts "#{e.message}"
            puts "#{e.backtrace.join("\n")}" 
        end
    end

    imported
  end
end
