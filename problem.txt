Problem

1 - Load the "dump.sql" file to a database
2 - Open "1139_2014_01_08.csv" file and process each line with the following:

    A - Get the first field, that represents a Unix time, and insert the value on the format "2000-01-01 00:00:00" (UTC) on table "run_detail_files", field "enter"
    B - Use the sixth field, that represents a mac address, find the related description on Node and insert its value (description) on table "run_detail_files", field "area"
    C - Use the same field above and using the association node -> stores, get the store name and insert its value on table "run_detail_files", field "store_name"
    D - None of the registers (line) of the file can be inserted if it's last field has a value lower than "-60"