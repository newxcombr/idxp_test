# -*- encoding : utf-8 -*-
require 'rubygems'
require File.join(Rails.root, 'config/environment')

desc "Import csv"
task :import_csv, [:filepath] do |t, args|
    csv_path = args[:filepath]
    threshold = -60

    total_imported = RunDetailFile.load_from_csv(csv_path, threshold)

    puts "#{total_imported} lines imported"
end
