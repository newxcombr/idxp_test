require 'spec_helper'

describe RunDetailFile do
  it "should load_from_csv" do
    RunDetailFile.delete_all
    csv_path = File.join Rails.root, "spec", "data", "test_data.csv"

    total_imported = RunDetailFile.load_from_csv(csv_path, -60.0)

    total_imported.should eql(5)
    RunDetailFile.all.size.should eql(5)
  end

  it "should format macaddress" do 
    str = "AC86740D6938"
    mac = str.scan(/.{1,2}/).join(":")
    expected_str = "AC:86:74:0D:69:38"
    mac.should eql(expected_str)

    str.to_macaddress.should eql(expected_str)
  end
end
