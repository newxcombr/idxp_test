## README

### Requirements

* Ruby 2.1.0
* Rails 4.1.0.beta1
* Mysql

### Setup Mysql

* create a database named "idxp_test" on mysql
* Load the dump file using the following command =>  mysql -u root -p database_name < dump.sql

### Executing / processing a .csv file

    bundle exec rake import_csv[/tmp/1139_2014_01_08.csv] RAILS_ENV=development

### Testing with Rspec

Go to app root directory (eg. /home/user/idxp_test) and run the following code

    rspec

