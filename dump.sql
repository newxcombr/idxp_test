/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_nodes_on_mac` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `run_detail_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) DEFAULT NULL,
  `enter` datetime DEFAULT NULL,
  `exit` datetime DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `area_unified` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `hour` int(11) DEFAULT NULL,
  `mac` varchar(255) DEFAULT NULL,
  `macs_per_day` int(11) DEFAULT NULL,
  `kind` varchar(255) DEFAULT NULL,
  `just_date` date DEFAULT NULL,
  `store_name` varchar(255) DEFAULT NULL,
  `store_area_node_id` int(11) DEFAULT NULL,
  `navizon_site_id` int(11) DEFAULT NULL,
  `run_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `duration_day_diff` int(11) DEFAULT NULL,
  `duration_day_sum` int(11) DEFAULT NULL,
  `sales_transaction_id` int(11) DEFAULT NULL,
  `store_area_id` int(11) DEFAULT NULL,
  `sales_path_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_run_detail_files_on_just_date_and_navizon_site_id` (`just_date`,`navizon_site_id`),
  KEY `index_run_detail_files_on_macs_per_day` (`macs_per_day`),
  KEY `index_run_detail_files_on_navizon_site_id` (`navizon_site_id`),
  KEY `index_run_detail_files_on_duration` (`duration`),
  KEY `run_detail_files_area` (`area`),
  KEY `run_detail_files_mac` (`mac`),
  KEY `index_run_detail_files_on_sales_transaction_id` (`sales_transaction_id`),
  KEY `index_run_detail_files_on_store_area_id` (`store_area_id`),
  KEY `index_run_detail_files_on_sales_path_id` (`sales_path_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chain_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `mode` int(11) DEFAULT NULL,
  `navizon_site_id` int(11) DEFAULT NULL,
  `time_zone` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'UTC',
  `algorithm_triangulation_type` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_stores_on_navizon_site_id` (`navizon_site_id`),
  KEY `index_stores_on_chain_id` (`chain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `store_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `cashier_area` tinyint(1) DEFAULT NULL,
  `departament_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_store_areas_on_store_id_and_area_id` (`store_id`,`area_id`),
  KEY `index_store_areas_on_store_id` (`store_id`),
  KEY `index_store_areas_on_area_id` (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `store_area_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_area_id` int(11) DEFAULT NULL,
  `node_id` int(11) DEFAULT NULL,
  `rss_min` int(11) DEFAULT NULL,
  `rss_max` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `rss_average` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_store_area_nodes_on_store_area_id_and_node_id` (`store_area_id`,`node_id`),
  KEY `index_store_area_nodes_on_node_id_and_rss_min_and_rss_max` (`node_id`,`rss_min`,`rss_max`),
  KEY `index_store_area_nodes_on_store_area_id` (`store_area_id`),
  KEY `index_store_area_nodes_on_node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` (`id`, `chain_id`, `name`, `address`, `contact`, `phone`, `created_at`, `updated_at`, `mode`, `navizon_site_id`, `time_zone`, `algorithm_triangulation_type`) VALUES
	(8, 4, 'Store 8', '', '', '', '2013-05-29 20:17:31', '2013-12-04 20:33:47', 1, 1139, 'Bern', 1);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;

/*!40000 ALTER TABLE `nodes` DISABLE KEYS */;
INSERT INTO `nodes` (`id`, `mac`, `description`, `store_id`, `created_at`, `updated_at`) VALUES
	(75, 'AC:86:74:0D:69:38', 'NT101', 8, '2013-05-29 20:37:26', '2013-05-29 20:37:26'),
	(76, 'AC:86:74:0D:69:18', 'NT102', 8, '2013-05-29 20:37:56', '2013-05-29 20:37:56'),
	(77, 'AC:86:74:0D:69:50', 'NT103', 8, '2013-05-29 20:38:18', '2013-05-29 20:38:18'),
	(78, 'AC:86:74:0D:69:00', 'NT104', 8, '2013-05-29 20:38:40', '2013-05-29 20:38:40'),
	(79, 'AC:86:74:0D:69:30', 'NT105', 8, '2013-05-29 20:38:58', '2013-05-29 20:38:58'),
	(80, 'AC:86:74:0D:68:B0', 'NT106', 8, '2013-05-29 20:39:18', '2013-05-29 20:39:18'),
	(81, 'AC:86:74:0D:68:B8', 'NT107', 8, '2013-05-29 20:39:42', '2013-05-29 20:39:42');
/*!40000 ALTER TABLE `nodes` ENABLE KEYS */;

/*!40000 ALTER TABLE `store_areas` DISABLE KEYS */;
INSERT INTO `store_areas` (`id`, `store_id`, `area_id`, `created_at`, `updated_at`, `cashier_area`, `departament_id`) VALUES
	(67, 8, 29, '2013-05-29 20:31:49', '2013-05-29 20:31:49', NULL, NULL),
	(68, 8, 30, '2013-05-29 20:32:03', '2013-05-29 20:32:03', NULL, NULL),
	(69, 8, 31, '2013-05-29 20:32:16', '2013-05-29 20:32:16', NULL, NULL),
	(70, 8, 32, '2013-05-29 20:32:27', '2013-05-29 20:32:27', NULL, NULL),
	(71, 8, 33, '2013-05-29 20:32:42', '2013-05-29 20:32:42', NULL, NULL),
	(73, 8, 34, '2013-05-29 20:33:11', '2013-05-29 20:33:11', NULL, NULL),
	(74, 8, 35, '2013-05-29 20:33:26', '2013-05-29 20:33:26', NULL, NULL);
/*!40000 ALTER TABLE `store_areas` ENABLE KEYS */;

/*!40000 ALTER TABLE `store_area_nodes` DISABLE KEYS */;
INSERT INTO `store_area_nodes` (`id`, `store_area_id`, `node_id`, `rss_min`, `rss_max`, `enabled`, `created_at`, `updated_at`, `rss_average`) VALUES
	(77, 67, 75, -60, 0, 1, '2013-05-29 20:44:52', '2013-08-28 19:00:06', NULL),
	(78, 68, 76, -60, 0, 1, '2013-05-29 20:45:14', '2013-08-28 19:00:11', NULL),
	(79, 69, 77, -60, 0, 1, '2013-05-29 20:45:38', '2013-08-28 19:00:16', NULL),
	(80, 70, 78, -60, 0, 1, '2013-05-29 20:46:03', '2013-08-28 19:00:19', NULL),
	(81, 71, 79, -60, 0, 1, '2013-05-29 20:47:21', '2013-08-28 19:00:22', NULL),
	(82, 73, 80, -60, 0, 1, '2013-05-29 20:47:47', '2013-08-28 19:00:25', NULL);
/*!40000 ALTER TABLE `store_area_nodes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
